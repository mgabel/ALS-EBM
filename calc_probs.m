function [log_xE,log_xnotE] = calc_probs(phat,data)
%CALC_PROBS returns log(p(x|E)) and log(p(x|~E)).
%   [LOG_XE,LOG_XnotE] = CALC_PROBS(PHAT,DATA) returns the log likelihood
%   (LLH) for each subject's biomarker reading given i) the biomarker is in
%   a diseased state, or ii) the biomarker is in a healthy state. These 
%   correspond to each Event occurring and not occurring, respectively.
%   
%   PHAT is a D-by-5 matrix of mixture parameters from FITMIX.m or FIXMIX.m.
%   DATA is an M-by-D matrix of patient biomarker readings (i.e. ALS data).

% % p(x|E)
x_given_E = NaN(size(data,1),size(data,2)); % Initialise x_given_E
for j = 1:size(data,1) % number of patients
    for n = 1:size(data,2) % number of events
        x_given_E(j,n) = normpdf(data(j,n),phat(n,3),phat(n,4));
    end
end

% % p(x|~E)
x_given_notE = NaN(size(data,1),size(data,2)); % Initialise x_given_notE
for j = 1:size(data,1) % number of patients
    for n = 1:size(data,2) % number of events
        x_given_notE(j,n) = normpdf(data(j,n),phat(n,1),phat(n,2));
    end
end

% % log(p(x|E)) and log(p(x|~E))
log_xE = log(x_given_E);
log_xnotE = log(x_given_notE);

% % % % Replace -Inf log probs
log_xE(log_xE == -Inf) = log(1e-251);
log_xnotE(log_xnotE == -Inf) = log(1e-251);

% % Replace missing data with 0.5 prob
log_xE(isnan(log_xE)) = log(0.5);
log_xnotE(isnan(log_xnotE)) = log(0.5);

end