function [LSE] = calculate_p_X_given_S(current_S,log_x_given_E,log_x_given_notE)
%CALCULATE_P_X_GIVEN_S returns ln(p(X|S)) for a given order S.
[J,N] = size(log_x_given_E);

% % p(Xj|S,k) (eq. 1) % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 

log_Xj_given_S_k = NaN(J,N+1); % Initialise Xj_given_S_k

% (p(x|~E) for all events i.e. no events have occurred
log_Xj_given_S_k(:,1) = sum(log_x_given_notE,2);
% Want ((p(x|E) for S(1:k) events) * (p(x|~E) for S(k+1:N) events))
for k = 1:N-1
    log_Xj_given_S_k(:,k+1) = sum(log_x_given_E(:,current_S(1:k)),2) + ...
        sum(log_x_given_notE(:,current_S(k+1:N)),2);
end
% and (p(x|E) for all events) i.e. all events have occurred
log_Xj_given_S_k(:,N+1) = sum(log_x_given_E,2);

% % p(Xj|S) (eq. 2) % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
Xj_given_S = sum(exp(log_Xj_given_S_k),2);

% % p(X|S) (eq. 3)% % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
LSE = sum(log((1/N)*Xj_given_S));

end