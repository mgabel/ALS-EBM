function [greedy_S_ML] = compare_greedy(log_xE,log_xnotE,varargin)
%COMPARE_GREEDY performs multiple runs of a greedy ascent algorithm on 
%   log(p(x|E)) and log(p(x|~E)).
%   
%   GREEDY_S_ML = COMPARE_GREEDY(LOG_XE,LOG_XnotE) returns greedy_S_ML, the 
%   most likely order of Events, which is used to initialise MCMC_PHASE.m.
% 
%   [...] = COMPARE_GREEDY(LOG_XE,LOG_XnotE,'PARAM1',val1,'PARAM2',val2,...) 
%   specifies one or more of the following name/value pairs:  
% 
%       Parameter               Value
%       'iterations'            Sets the number of iterations for each run 
%                               of the greedy ascent algorithm. Default is
%                               5000.
%
%       'startpoints'           Sets the number of startpoints, i.e. the 
%                               number of runs. Default is 32.
% 
%       'plot_figure'           Boolean.  If true, plots the log-likelihood
%                               against the iteration number for each run.


% Parse inputs
p = inputParser;
addOptional(p,'iterations',5000);
addOptional(p,'startpoints',32);
addOptional(p,'plot_figure',1);
p.parse( varargin{:} )

iterations = p.Results.iterations;
startpoints = p.Results.startpoints;
plot_figure = p.Results.plot_figure;

greedy_S_ML_temp = NaN(startpoints,size(log_xE,2)+1);
greedy_plot_temp = NaN(startpoints,iterations)';

% Plot figure if required
if plot_figure == true
    figure;
    hold on;
    xlabel('Iterations')
    ylabel('log-likelihood')
end    

% Loop over the greedy ascent runs
for i = 1:startpoints
    S_initial = randperm(size(log_xE,2));
    [~, S] = greedy_ascent(S_initial,iterations,log_xE,log_xnotE);
    greedy_S_ML_temp(i,:) = S(end,:);
    greedy_plot_temp(:,i) = S(2:end,end);
    if plot_figure == true
        plot(S(1:end-1,end)) % plot greedy ascent trace
    end
end

% Return the most likely Event order 
[~, idx] = max(greedy_S_ML_temp(:,end));
greedy_S_ML = greedy_S_ML_temp(idx,1:end);


end

%%
function [greedy_S_ML, S] = greedy_ascent(S_initial,iterations,log_x_given_E,log_x_given_notE)
%GREEDY_ASCENT calculates the LLH for an initial Event order, then swaps
%   the position of two Events.  The new order is retained if it has a
%   higher LLH.

% Pre-allocation
N = size(log_x_given_E,2);
S = NaN(iterations+1,N+1);

% Starting 
S(1,1:N) = S_initial;
S(1,N+1) = calculate_p_X_given_S(S_initial,log_x_given_E,log_x_given_notE);

for k = 1:iterations
    
    swap = randperm(N,2);
    
    % Generate S'
    S(k+1,1:N) = S(k,1:N); % Copy S_t
    S(k+1,swap(1)) = S(k,swap(2)); % Swap random event 1
    S(k+1,swap(2)) = S(k,swap(1)); % Swap random event 2
    
    % Calculate p(X|S')
    S(k+1,N+1) = calculate_p_X_given_S(S(k+1,1:N),log_x_given_E,log_x_given_notE);
    
    % If p(X|S') < p(X|S_t), then set S_t+1 = S_t
    if S(k+1,N+1) < S(k,N+1)
        S(k+1,1:N+1) = S(k,1:N+1);
    end
    % otherwise, leave S_t+1 = S'
end

% Find the order with the maximum likelihood
[row, ~] = find(S == max(S(:,end)));
greedy_S_ML = S(max(row),1:N+1);

end