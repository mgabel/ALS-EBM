function [phat, phat_con] = fit_mix(labels,CTL,ALS,varargin)
%FITMIX Fit constrained Gaussian mixtures to biomarker data.
%   [PHAT] = FITMIX(LABELS,CTL,ALS) fits constrained two-component Gaussian
%   mixtures to the data [CTL;ALS].  CTL is an N-by-D matrix, and ALS is an
%   M-by-D matrix, where N can equal M. Rows of CTL and ALS correspond to
%   observations; columns correspond to biomarkers.  LABELS is a 1-by-D
%   cell list of biomarker names.
%
%   FITMIX excludes NaNs on a per-biomarker basis.
%
%   [PHAT] = FITMIX(LABELS,CTL,ALS,'PARAM1',val1, 'PARAM2',val2, ...)
%   allows you to specify optional parameter name/value pairs to specify
%   details of the model fitting process.
%   Parameters are:
%
%       'mix_thresh'            Sets the threshold for mixing proportion.
%                               Fitting will satisfy 'mix_thresh <
%                               ComponentProportion < 1 - mix_thresh'.
%                               The range is (0,1). Default is 0.05.
%
%       'outlier_thresh'        Number of SDs to use to determine outliers.
%                               Set to 2 for the usual 95.45% CI, or 3 for
%                               99.73% CI. Default is 7 SDs; this should
%                               give no outliers.
%
%       'reg_inc'               Incremental increase of the non-negative
%                               regularisation value for the mixture
%                               fitting covariance matrix, as proportion of
%                               min(var(CTL),var(ALS)). Default is 0.01.
%
%       'iterations'            Number of iterations of EM algorithm for
%                               for mixture fitting. Default is 5000.
%
%       'plot_figure'           Boolean. If true, then will plot histograms
%                               of the data with distributions overlaid.
%                               Default is 1.
%
%       'verbose'               Boolean. If true, will display a warning
%                               when patient or control data are
%                               non-normally distributed, as determined by
%                               Lilliefors' composite goodness-of-fit test.
%                               Default is 1.

% Parse inputs
p = inputParser;
addOptional(p,'mix_thresh',0.05);
addOptional(p,'outlier_thresh',7);
addOptional(p,'reg_inc',0.01);
addOptional(p,'iterations',5000);
addOptional(p,'plot_figure',1);
addOptional(p,'verbose',1);
p.parse( varargin{:} )

threshold = p.Results.mix_thresh;
outlier_thresh = p.Results.outlier_thresh;
reg_inc = p.Results.reg_inc;
iterations = p.Results.iterations;
plot_figure = p.Results.plot_figure;
verbose = p.Results.verbose;

% Dummy variable for ignoring outliers
oCTL = CTL;
oALS = ALS;

% Fit two-component Gaussian mixtures
for j = 1:size(labels,2)
    biomarker = matlab.lang.makeValidName(char(labels(j)));
    % Ignore NaNs
    nCTL = CTL(~isnan(CTL(:,j)),j);
    nALS = ALS(~isnan(ALS(:,j)),j);
    
    reg_value = min(var(nCTL),var(nALS));
    reg_thresh = reg_inc;
    
    % Fit single Gaussian to CTL data
    [mu_c, sig_c] = normfit(nCTL);
    % Use single Gaussian to remove outliers for fitting
    oCTL( (CTL(:,j) < (mu_c - outlier_thresh*sig_c)),j ) = NaN;
    oCTL( (CTL(:,j) > (mu_c + outlier_thresh*sig_c)),j ) = NaN;
    
    % Display warning if CTL data are non-Gaussian
    if lillietest(oCTL(:,j)) == 1 && verbose == 1
        fprintf('%s CTL not Gaussian\n',char(labels(j)))
    end
    
    % Fit single Gaussian to ALS data
    [mu_a, sig_a] = normfit(nALS);
    % Use single Gaussian to remove outliers for fitting
    oALS( (ALS(:,j) < (mu_a - outlier_thresh*sig_a)),j ) = NaN;
    oALS( (ALS(:,j) > (mu_a + outlier_thresh*sig_a)),j ) = NaN;
    
    % Display warning if ALS data are non-Gaussian
    if lillietest(oALS(:,j)) == 1 && verbose == 1
        fprintf('%s ALS not Gaussian\n',char(labels(j)))
    end
    
    % Create patient/control status variable to initialise mixture fitting
    status = [ones(sum(~isnan(oCTL(:,j))),1); ...
        2*ones(sum(~isnan(oALS(:,j))),1)]';
    
    % Fit constrained mixture
    phat_con.(biomarker) = fitgmdist([oCTL(~isnan(oCTL(:,j)),j);...
        oALS(~isnan(oALS(:,j)),j)],2,...
        'Regularize',reg_value*reg_thresh,'Start',status,...
        'Options',statset('MaxIter',5000));
    % If mixing proportion is not within requirements, increase
    % regularisation value by specified increment, and refit
    while phat_con.(biomarker).ComponentProportion(1) < threshold ...
            || phat_con.(biomarker).ComponentProportion(1) > (1 - threshold)
        reg_thresh = reg_thresh + reg_inc;
        phat_con.(biomarker) = fitgmdist([oCTL(~isnan(oCTL(:,j)),j);...
            oALS(~isnan(oALS(:,j)),j)],2,...
            'Regularize',reg_value*reg_thresh,'Start',status,...
            'Options',statset('MaxIter',iterations));
    end
    
end

% Extract mixture parameters from GMM objects
phat = NaN(size(labels,2),5); % Initialise phat
for j = 1:size(labels,2)
    biomarker = matlab.lang.makeValidName(char(labels(j)));
    % Extract mu_c and mu_a
    phat(j,1) = phat_con.(biomarker).mu(1);
    phat(j,3) = phat_con.(biomarker).mu(2);
    
    % Calculate sigma from variance
    phat(j,2) = sqrt(phat_con.(biomarker).Sigma(1));
    
    if phat_con.(biomarker).SharedCovariance == 0
        % Calculate sigma from variance
        phat(j,4) = sqrt(phat_con.(biomarker).Sigma(2));
    else
        phat(j,4) = phat(j,2); % as shared covariance
    end
    
    % Extract mixing proportion
    phat(j,5) = phat_con.(biomarker).ComponentProportion(1);
end

% Plot fitted mixtures if required
if plot_figure == 1
    plot_fitted_mixtures([CTL;ALS],phat_con,labels)
end

end